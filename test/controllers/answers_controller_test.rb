require 'test_helper'

class AnswersControllerTest < ActionController::TestCase
  setup do
    @answer = answers(:one)
  end

  test "should create answer" do
    assert_difference('Answer.count') do
      post :create, answer: { question_id: @answer.question_id, text: @answer.text, user_id: @answer.user_id }
    end

    assert_redirected_to answer_path(assigns(:answer))
  end

  test "should update answer" do
    patch :update, id: @answer, answer: { question_id: @answer.question_id, text: @answer.text, user_id: @answer.user_id }
    assert_redirected_to answer_path(assigns(:answer))
  end

  test "should destroy answer" do
    assert_difference('Answer.count', -1) do
      delete :destroy, id: @answer
    end

    assert_redirected_to answers_path
  end
end
