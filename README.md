# “OutOfMemory” #
## Simple [Stackoverflow](http://stackoverflow.com/) clone. ##

Демо доступно по адрессу: [out-of-memory.herokuapp.com](http://out-of-memory.herokuapp.com/)

-----

**Задание:** сделать примитивный аналог сайта [stackoverflow.com](http://stackoverflow.com/), [askdev.ru](http://askdev.ru), [hashcode.ru](http://hashcode.ru)

-----

**Функционал:**

1. Простая регистрация


2. Авторизация (обычная и через один из сторонних сервисов: Facebook, Vkontakte, Google и т. п., выбрать понравившийся).


3. Главная страница

    Содержит список вопросов, упорядоченные по дате добавления. Самые свежие вверху. Есть постраничная навигация.
    
    Каждый из вопросов представлен следующей информацией:
    
    - тема
    - рейтинг, число ответов, число просмотров
    - имя пользователя, аватарка пользователя
    - теги
    
    На главной странице должно быть облако тегов
    
    
4. Страница вопроса

    Содержит детальное описание вопроса, ответы и комментарии к ним
    
    Каждый ответ имеет свой рейтинг. Ответы отсортированы по-убыванию рейтинга.
    
    После всех ответов располагается форма для написания ответа
    
    Отвечать, комментировать, голосовать могут только авторизованные пользователи
    
    Голосование и комментирование реализовать с помощью AJAX.

    
5. *API (для фанатов своего дела;)).

    Реализовать простое API для получения информации с сайта в формате JSON (данный функционал может использоваться сторонними сервисами или приложениями, например мобильными устройствами)
    
    - список вопросов, например по запросу `http://yoursite.com/api/question` 
    
    - информацию о конкретном вопросе вместе с комментариями и ответами, например по запросу `http://yoursite.com/api/question/1` (информация о вопросе с id=1)
    
-----

Для работы сайта необходимо установить значения для следующих переменных среды:

Переменная | Описание
----------------- | -------------
`MAIL_USERNAME` | Имя пользователя [Gmail](https://mail.google.com)
`MAIL_PASSWORD` | Пароль для почты [Gmail](https://mail.google.com)
`RECAPTCHA_PUBLIC_KEY` | Открытый ключ [ReCaptcha](https://www.google.com/recaptcha/intro/index.html)
`RECAPTCHA_PRIVATE_KEY` | Закрытый ключ [ReCaptcha](https://www.google.com/recaptcha/intro/index.html)
`CLOUDINARY_CLOUD_NAME` | Название облака [Cloudinary](http://cloudinary.com/)
`CLOUDINARY_API_KEY` | Ключ API [Cloudinary](http://cloudinary.com/)
`CLOUDINARY_API_SECRET` | Секретный ключ API [Cloudinary](http://cloudinary.com/)
`GOOGLE_OAUTH2_CLIENT_ID` | ID клиента [Google OAuth](https://console.developers.google.com/project)
`GOOGLE_OAUTH2_CLIENT_SECRET` | Секретный ключ клиента [Google OAuth](https://console.developers.google.com/project)
