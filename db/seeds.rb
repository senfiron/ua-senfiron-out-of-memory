# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Users
99.times do |n|
  username = Faker::Internet.user_name
  email = Faker::Internet.safe_email(username)
  password = 'password'
  user = User.new(
           email:                 email,
           username:              username,
           password:              password,
           password_confirmation: password,
           confirmed_at: Time.zone.now)
  user.skip_confirmation!
  if user.valid?
    user.save!
  end
end
users = User.all

# Avatars
users.each do |user|
  user.remote_avatar_url = Faker::Avatar.image
  user.save!
end

rnd = Random.new
users_count = users.count

#Questions

# 1 question per user average
users_count.times do
  users[rnd.rand(users_count)].questions.create!(
      title: Faker::Hacker.say_something_smart,
      text: Faker::Lorem.paragraph(6),
      tag_list: Faker::Hacker.adjective
  )
end

#Answers

Question.first(10).each do |q|
  q.tag_list = Array.new(5){Faker::Hacker.adjective}.join(', ')
  q.save!
  rnd.rand(10).times do
    u = users[rnd.rand(users_count)]
    a = q.answers.create!(user: u, text: Faker::Lorem.sentence(6))
    u.vote(q, direction: [:up, :down].sample, exclusive: true)

    #Comments to answers
    rnd.rand(5).times do
      u = users[rnd.rand(users_count)]
      a.comments.create!(user: u, text: Faker::Lorem.sentence(6))
      u.vote(a, direction: [:up, :down].sample, exclusive: true)
    end
  end

  #Comments to questions
  rnd.rand(10).times do
    u = users[rnd.rand(users_count)]
    q.comments.create!(user: u, text: Faker::Lorem.sentence(6))
    u.vote(q, direction: [:up, :down].sample, exclusive: true)
  end
end