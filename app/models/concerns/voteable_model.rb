module VoteableModel
  extend ActiveSupport::Concern

  included do
    acts_as_voteable

    # plusminus_tally method from gem doesn't work with pagination
    # using scope fixes preload of votes
    scope :includes_votes, -> { select("#{table_name}.*")
        .select("SUM(CASE votes.vote WHEN 't' THEN 1 WHEN 'f' THEN -1 ELSE 0 END) AS plusminus_tally")
        .joins("LEFT OUTER JOIN votes ON #{table_name}.id = votes.voteable_id AND votes.voteable_type = '#{model_name}'")
        .group("#{table_name}.id") }
    scope :by_rating, -> { order 'plusminus_tally DESC'}
  end
end
