class User < ActiveRecord::Base

  default_scope -> { order(:id) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  mount_uploader :avatar, AvatarUploader
  validates_integrity_of  :avatar
  validates_processing_of :avatar
  validates :username, uniqueness: { case_sensitive: false }, allow_nil: true

  acts_as_voter

  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :comments, dependent: :destroy

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  # Don't allow to change email w/o password
  def update_without_password(params, *options)
    params.delete(:email)
    super(params, *options)
  end

  # Sign in or Sign up with Google oauth2
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data.email).first

    unless user
      generated_password = Devise.friendly_token.first(8)
      user = User.new(
          email: data.email,
          password: generated_password
      )
      user.skip_confirmation!
      user.save
      UserMailer.welcome(user, generated_password).deliver
    end
    user
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(['lower(username) = :value OR lower(email) = :value', {value: login.downcase}]).first
    else
      where(conditions).first
    end
  end
end
