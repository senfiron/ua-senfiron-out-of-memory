class Question < ActiveRecord::Base
  include VoteableModel
  include FormatText

  belongs_to :user
  has_many :answers, dependent: :destroy
  has_many :comments, as: :commentable

  acts_as_taggable

  is_impressionable counter_cache: true, unique: [:impressionable_type, :impressionable_id, :ip_address]

  default_scope -> { order('created_at DESC') }

  before_validation :format_text, on: [:create, :update]

  validates :user, presence: true
  validates :text, presence: true
  validates :title, presence: true
  validates :tag_list, presence: true

  validate :max_tag_size

  private
    def max_tag_size
      return if self.tag_list.empty?
      errors[:tag_list] << '5 tags maximum' if self.tag_list.count > 5
      errors[:tag_list] << 'can\'t contain tag with length < 20 letters ' if self.tag_list.collect(&:length).max >= 20
    end

end
