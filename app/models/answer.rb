class Answer < ActiveRecord::Base
  include VoteableModel
  include FormatText

  belongs_to :question, counter_cache: true
  belongs_to :user
  has_many :comments, as: :commentable

  before_validation :format_text, on: [:create, :update]

  validates :user, presence: true
  validates :text, presence: true

end
