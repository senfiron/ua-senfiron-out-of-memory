class CommentsController < ApplicationController
  before_filter :authenticate_user!

  respond_to :html
  respond_to :js, only: [:create, :destroy]

  def create
    @commentable = find_commentable
    @comment = @commentable.comments.new(comment_params)
    @comment.user = current_user
    flash[:notice] = 'Successfully commented.' if @comment.save

    @comments_container = "#{@commentable.class.name.first.downcase}#{@commentable.id}-comments"

    respond_with(@commentable)
  end

  private
    def comment_params
      params.require(:comment).permit(:text)
    end

    def find_commentable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          return $1.classify.constantize.find(value)
        end
      end
      nil
    end
end
