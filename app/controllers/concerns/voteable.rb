module Voteable extend ActiveSupport::Concern

  def vote
    voteable_class = controller_name.singularize.capitalize.constantize
    voteable_instance = voteable_class.find(params[:id])

    if current_user.voted_on?(voteable_instance)
      current_user.unvote_for(voteable_instance)
    else
      current_user.vote(voteable_instance, direction: params[:type], exclusive: true)
    end

    @voted_how = current_user.voted_how? voteable_instance
    @counter_value = voteable_instance.plusminus
    @counter_container = "#{controller_name.first}#{params[:id]}_votes"

    respond_to do |format|
      format.js
      format.html { redirect_to :back }
    end
  end

end
