module FormatText extend ActiveSupport::Concern

  protected

    def format_text
      result = ''
      code_started = false
      ERB::Util::h(self.text).each_line do |line|
        if line.start_with? '    '
          unless code_started
            code_started = true
            result << '<pre><code>'
          end
          line = line[4..-1]
        elsif code_started
          result << '</pre></code>'
          code_started = false
        end
        result << line if line
      end
      result << '</pre></code>' if code_started
      self.text = result
    end
end
