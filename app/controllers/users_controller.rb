class UsersController < ApplicationController

  # List of users
  def index
    @users = User.page params[:page]
  end

  # User's profile
  def show
    @user = User.find(params[:id])
  end

end
