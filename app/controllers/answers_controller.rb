class AnswersController < ApplicationController
  include Voteable

  before_filter :authenticate_user!, except: [:index, :show]
  before_action :set_question, only: [:create]
  before_action :set_answer, only: [:edit, :update, :destroy]

  respond_to :html
  respond_to :js, only: [:create, :destroy]

  def edit
  end

  def create
    @answer = @question.answers.new(answer_params)
    @answer.user = current_user
    flash[:notice] = 'Successfully answered.' if @answer.save
    respond_with(@answer.question)
  end

  def update
    flash[:notice] = 'Answer was successfully updated.' if @answer.update(answer_params)
    respond_with(@answer.question)
  end

  def destroy
    @answer.destroy
    flash[:notice] = 'Answer was successfully deleted.' unless @answer.errors.any?
    respond_with(@answer.question)
  end

  private
    def set_question
      @question = Question.find(params[:question_id])
    end

    def set_answer
      @answer = Answer.find(params[:id])
    end

    def answer_params
      params.require(:answer).permit(:text, :question_id, :user_id)
    end
end
