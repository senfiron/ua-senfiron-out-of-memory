class QuestionsController < ApplicationController
  include Voteable

  before_filter :authenticate_user!, except: [:index, :show]
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_filter :check_user, only: [:edit, :update, :destroy]

  impressionist actions: [:show], unique: [:impressionable_type, :impressionable_id, :ip_address]

  respond_to :html
  respond_to :json, only: [:index, :show]

  def index
    @questions = Question.includes_votes.includes(:tags).page params[:page]
    @questions = @questions.tagged_with(params[:tag]) if params[:tag]

    respond_with(@questions)
  end

  def show
    respond_with(@question)
  end

  def new
    @question = Question.new
    respond_with(@question)
  end

  def edit
  end

  def create
    @question = Question.new(question_params)
    @question.user = current_user
    flash[:notice] = 'Question was successfully created.' if @question.save
    respond_with(@question)
  end

  def update
    flash[:notice] = 'Question was successfully updated.' if @question.update(question_params)
    respond_with(@question)
  end

  def destroy
    @question.destroy
    respond_with(@question)
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end

    def check_user
      unless current_user.id == @question.user.id
        flash[:alert] = 'You must be the owner of this question.'
        redirect_to @question
      end
    end

    def question_params
      params.require(:question).permit(:title, :text, :user_id, :tag_list)
    end
end
