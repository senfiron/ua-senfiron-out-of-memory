json.extract! @question, :id, :title, :text, :created_at, :updated_at
json.user { json.partial! 'users/user', user: @question.user }
json.comments @question.comments, partial: 'comments/comment', as: :comment
json.answers @question.answers, partial: 'answers/answer', as: :answer
json.list api_questions_url
