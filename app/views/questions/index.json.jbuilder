json.array!(@questions) do |question|
  json.extract! question, :id, :title, :user_id
  json.user { json.partial! 'users/user', user: question.user }
  json.url api_question_url(question)
end
