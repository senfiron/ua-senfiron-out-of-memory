json.extract! comment, :id, :text, :created_at, :updated_at
json.user { json.partial! 'users/user', user: comment.user }
