json.extract! answer, :id, :text, :created_at, :updated_at
json.user { json.partial! 'users/user', user: answer.user }
json.comments @question.comments, partial: 'comments/comment', as: :comment
