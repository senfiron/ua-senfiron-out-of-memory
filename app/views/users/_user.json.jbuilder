json.extract! user, :id, :email
json.avatar do
  json.url user.avatar.url
  json.thumb_url user.avatar.thumb.url
end
