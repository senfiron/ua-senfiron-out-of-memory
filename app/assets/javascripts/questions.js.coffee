# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

highlight = ->
  $('pre > code').each (i, block) ->
    hljs.highlightBlock block

ready = ->
  $('#answer-form form').on 'ajax:success', ->
    highlight()
  highlight()

$(document).ready(ready);
$(document).on('page:load', ready);
