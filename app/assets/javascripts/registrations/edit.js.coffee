ready = ->
  $('#myTab a').click = (e) ->
    e.preventDefault()
    $(this).tab('show')
  $('[data-toggle="tooltip"]').tooltip({container: 'body'})

$(document).ready(ready);
$(document).on('page:load', ready);
