class UserMailer < ActionMailer::Base
  default from: ENV['MAIL_USERNAME']

  def welcome(user, password)
    @user = user
    @password = password
    @url  = new_user_session_url
    mail(to: @user.email, subject: 'Welcome to OutOfMemory')
  end
end
